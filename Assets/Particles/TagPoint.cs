﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TagPoint : MonoBehaviour
{

    public GameObject particulas;
    public bool tagged = false;

	void Update ()
    {
        if (tagged)
        {
            particulas.SetActive(true);
        }
        else
        {
            particulas.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.name);
        if(other.tag == "Player")
        {
            if(tagged)
            {
                tagged = false;
                other.transform.GetComponent<Player>().tagPoint.tagged = true;
            }
        }
    }
}
