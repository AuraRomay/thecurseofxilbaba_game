﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boomerang : MonoBehaviour
{
    Vector3 direction;
    public bool thrown = false;
    bool hit = false;
    public Transform snapPoint;
    public float speed;
    public string index;

    public void SetDirection(Vector3 direction)
    {
        
        if(this.direction == Vector3.zero)
        {
            this.direction = snapPoint.forward;           
        }

        Vector3.Normalize(this.direction);
        thrown = true;
    }

    private void Update()
    {
        if (thrown)
        {
            if(hit)
                direction = Vector3.Normalize(snapPoint.position - transform.position);

            transform.position = new Vector3(transform.position.x + direction.x * Time.deltaTime * speed, transform.position.y, transform.position.z + direction.z * Time.deltaTime * speed);
        }   
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "obstacle")
        {
            snapPoint.GetComponent<SphereCollider>().enabled = true;
            hit = true;
        }
        
        else if(other.gameObject.tag == "snapPoint")
        {
            thrown = false;
            hit = false;
            direction = Vector3.zero;
            snapPoint.GetComponent<SphereCollider>().enabled = false;
            transform.SetParent(snapPoint.transform);
            transform.localPosition = Vector3.zero;
            transform.localEulerAngles = Vector3.zero;
            transform.localScale = Vector3.one;
        }

    }
}
