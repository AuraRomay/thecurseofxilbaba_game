﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehavior : MonoBehaviour
{

    public GamePadManager playerManager;
    public Transform mapCenter;
    float distance;
    float[] distanceArray;

	void Start ()
    {
        distanceArray = new float[playerManager.players.Length];
	}
	
	void Update ()
    {

        float distanceSum = 0;

        for (int i = 0; i < distanceArray.Length; ++i)
        {
            distanceArray[i] = Vector2.Distance(mapCenter.position, playerManager.players[i].transform.position);
            distanceSum += distanceArray[i];
        }

        Debug.Log("Distance Sum: "  + distanceSum);

        distance = distanceSum / distanceArray.Length;
        Debug.Log("Distance: " + distance);

        transform.position = transform.forward * (-distance - 60);
    }

}
