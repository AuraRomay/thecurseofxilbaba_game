﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;

public class GamePadManager : MonoBehaviour
{

    public GameObject[] players;

    private void Awake()
    {
        players = GameObject.FindGameObjectsWithTag("Player");
        CheckForGamePads();
    }

    void CheckForGamePads()
    {

        for (int i = 0; i < players.Length; ++i)
        {
            PlayerIndex testPlayerIndex = (PlayerIndex)i;
            GamePadState testState = GamePad.GetState(testPlayerIndex);
            if (testState.IsConnected)
            {
                players[i].GetComponent<Player>().playerIndex = testPlayerIndex;
                players[i].GetComponent<Player>().playerIndexSet = true;

            }
        }
    }
}
