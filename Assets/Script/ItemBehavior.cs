﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBehavior : MonoBehaviour
{

    Vector3 originalPosition;
    public float speed, range;

    private void Start()
    {
        originalPosition = transform.position;
    }

    void Update ()
    {
        transform.position = new Vector3(originalPosition.x, originalPosition.y + Mathf.Sin(Time.fixedTime * speed) * range, originalPosition.z);		
	}
}
