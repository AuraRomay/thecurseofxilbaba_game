﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using XInputDotNetPure;

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, zMin, zMax;
}

public class Player : MonoBehaviour
{

    Vector3 direction;
    Animator animator;
    public float speed;
    public Boundary boundary;
    Rigidbody rb;
    public Boomerang boomerang;

    float moveHorizontal;
    float moveVertical;

    GamePadState state;
    GamePadState prevState;
    public bool playerIndexSet = false;
    public PlayerIndex playerIndex;

    public string playerNumber;

    public TagPoint tagPoint;
    public Text textScore;
    private int points;
    public int scorePoints = 1;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        playerNumber = playerIndex.ToString();
        boomerang.index = playerNumber;
        animator = GetComponent<Animator>();
        points = 0;
        textScore.text = points.ToString();
    }

    private void Update()
    {
        prevState = state;
        state = GamePad.GetState(playerIndex);

        if(GamePad.GetState(playerIndex).Buttons.B == ButtonState.Pressed && !boomerang.thrown)
        {

            boomerang.transform.SetParent(null);
            boomerang.SetDirection(new Vector3(moveHorizontal, 0, moveVertical));
            GamePad.SetVibration(playerIndex, 0.3f, 0.5f);
            StartCoroutine(StopHaptics());
        }

        ScoreSum();
    }

    IEnumerator StopHaptics()
    {
        yield return new WaitForSeconds(0.1f);
        GamePad.SetVibration(playerIndex, 0.0f, 0.0f);
    }

    IEnumerator Heal()
    {
        yield return new WaitForSeconds(0.5f);
        animator.SetBool("tagged", false);
    }

    void FixedUpdate()
    {
        moveHorizontal = GamePad.GetState(playerIndex).ThumbSticks.Left.X;
        moveVertical = GamePad.GetState(playerIndex).ThumbSticks.Left.Y;

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        rb.velocity = movement * speed;

        if (movement.magnitude != 0)
            animator.SetBool("moving", true);

        else
            animator.SetBool("moving", false);


        if (GamePad.GetState(playerIndex).Buttons.Y == ButtonState.Pressed)
            rb.AddForce(rb.velocity * 100);

        rb.position = new Vector3
        (
            Mathf.Clamp(rb.position.x, boundary.xMin, boundary.xMax),
            transform.position.y,
            Mathf.Clamp(rb.position.z, boundary.zMin, boundary.zMax)
        );

        //rigidbody.rotation = Quaternion.Euler(0.0f, 0.0f, rigidbody.velocity.x * -tilt);
        transform.LookAt(new Vector3(moveHorizontal + transform.position.x, transform.position.y, moveVertical + transform.position.z));

        //GamePad.SetVibration(playerIndex, state.Triggers.Left, state.Triggers.Right);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "boomerang")
        {
            Boomerang enemyBoomerang = other.GetComponent<Boomerang>();
            if(enemyBoomerang != null && enemyBoomerang.index != boomerang.index)
            {
                GamePad.SetVibration(playerIndex, 0.7f, 0.7f);
                StartCoroutine(StopHaptics());
                animator.SetBool("tagged", true);
                StartCoroutine(Heal());
            }
        }
    }

    void ScoreSum()
    {
        if (!tagPoint.tagged)
        {
            points+= scorePoints;
            textScore.text = points.ToString();
        }
    }

}
